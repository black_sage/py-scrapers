'''
Exercise file downloader for Pluralsight.
Put links to courses on links.txt

Use gecko driver (Firefox),
place the gecko driver in the current directory.

'''

from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile as FirefoxProfile
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import time
import os

username = ''
password = ''


def configure_firefox_driver():
    '''
    Source : https://www.pluralsight.com/guides/implementing-web-scraping-with-selenium\
           : https://selenium-python.readthedocs.io/faq.html#how-to-auto-save-files-using-custom-firefox-profile
    '''

    profile = FirefoxProfile()
    profile.set_preference("browser.download.folderList", 2)
    profile.set_preference("browser.download.manager.showWhenStarting", False)
    profile.set_preference("browser.download.dir", os.getcwd())
    profile.set_preference(
        "browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")

    # Add additional Options to the webdriver
    #firefox_options = FirefoxOptions()
    # add the argument and make the browser Headless.
    # firefox_options.add_argument("--headless")

    # Instantiate the Webdriver: Mention the executable path of the webdriver you have downloaded
    # if driver is in PATH, no need to provide executable_path
    # driver = webdriver.Firefox(executable_path = "./geckodriver.exe", options = firefox_options)
    driver = webdriver.Firefox(
        executable_path="./geckodriver", firefox_profile=profile)
    return driver


def login(driver):
    try:

        WebDriverWait(driver, 80).until(
            lambda s: s.find_element_by_xpath(
                '//*[@id="Username"]').is_displayed()
        )
    except ElementNotVisibleException:
        driver.close()

    input_username = driver.find_element_by_xpath('//*[@id="Username"]')
    input_password = driver.find_element_by_xpath('//*[@id="Password"]')
    btn_signin = driver.find_element_by_xpath('//*[@id="login"]')

    input_username.send_keys(username)
    input_password.send_keys(password)

    btn_signin.click()


def download_exercise_file(driver, url):

    DOWNLOAD_EXERCISE_FILE = r'//*[@id="ps-main"]/div/div[2]/section/div[3]/div/div/button'

    driver.get(url)
    try:
        WebDriverWait(driver, 10).until(
            lambda s: s.find_element_by_xpath(
                '/html/body/div[1]/div[3]/div/div[2]/section/div[3]/div/div/button').is_displayed()
        )
        btn_download = driver.find_element_by_xpath(DOWNLOAD_EXERCISE_FILE)
        btn_download.click()

    except (TimeoutException, NoSuchElementException) as e:
        try:
            btn_download = driver.find_element_by_xpath(
                '/html/body/div[1]/div[3]/div/div[2]/section/div[4]/div/div/button')
            btn_download.click()
        except (TimeoutException, NoSuchElementException) as e:
            print(f'{url} not downloaded.')

    # btn_download = driver.find_element_by_xpath(
    #    '/html/body/div[1]/div[3]/div/div[2]/section/div[3]/div/div/button')


# Setting up selenium for Firefox
driver = configure_firefox_driver()
# Signin Page
driver.get('https://www.pluralsight.com/id')

# Login using the above credentials
login(driver)

with open('links.txt', 'r') as f:
    link = f.readlines()

link = [x.strip() for x in link]
for l in link:
    if 'table-of-contents' in l:
        l = l.replace('table-of-contents', 'exercise-files')
    else:
        l = l + '/exercise-files'
    download_exercise_file(driver, l)
    time.sleep(30)

while True:
    c = input('Quit')
    if c == 'y':
        break

driver.close()
