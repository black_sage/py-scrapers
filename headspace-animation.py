'''
Downloads the Animation video from headspace.
'''
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotInteractableException, WebDriverException
from bs4 import BeautifulSoup
import time
import os
import subprocess
import traceback


username = ''
password = ''


def configure_firefox_driver():
    driver = webdriver.Firefox(
        executable_path="./geckodriver"
    )
    return driver


def login(driver, url):
    try:
        driver.get(url)
        input_username = driver.find_element_by_xpath(
            '//input[@type="email"]')
        input_password = driver.find_element_by_xpath(
            '//input[@type="password"]')
        btn_signin = driver.find_element_by_xpath(
            '//button[@type="submit"]')

        input_username.send_keys(username)
        input_password.send_keys(password)

        btn_signin.click()

    except TimeoutException:
        time.sleep(15)
        login(driver, url)


driver = configure_firefox_driver()
driver.implicitly_wait(10)  # seconds
# Login
login(driver, 'https://www.headspace.com/login')
time.sleep(10)

driver.get('https://my.headspace.com/discover/animations')


driver.find_element_by_xpath(
    '//button[@class="css-1u87ro6"]').click()


time.sleep(10)
animation_videos = driver.find_elements_by_xpath('//div[@class="css-1vkz7vb"]')

for anim in animation_videos:
    print(anim)
    anim.click()

    time.sleep(10)

    video = driver.find_elements_by_xpath('//video')

    subprocess.run(['aria2c', '-c', '--max-download-limit',
                    '10M', video.get_attribute('src')])

    print('Sleeping for 30 seconds')
    time.sleep(30)

    back_btn = driver.find_element_by_xpath(
        '//div[@class="css-16q6num"]//div[@data-component="svg-icon"]')
    back_btn.click()

driver.close()
